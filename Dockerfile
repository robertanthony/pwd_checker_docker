FROM node:15
WORKDIR /usr/src/app
COPY package.json ./
COPY yarn.lock ./
RUN yarn cache clean
RUN yarn
COPY . .
EXPOSE 5000
CMD [ "node", "index.js" ]
