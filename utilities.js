import minimist from "minimist"

const digitTester = /\d/g;
const capTester = /[A-Z]/g;

const argv = minimist(process.argv.slice(2));


const dict  = {
  length:10,
  must_have_numbers: true,
  must_have_caps: true
}

export function passwordCheck(password) {
  if (!password) return false;
  if (typeof password !== "string") return false;
  if (password.length < dict.length) return false; // assumes that 10 is a MINIMUM length
  if (dict.must_have_numbers && !digitTester.test(password)) return false;
  return !(dict.must_have_caps && !capTester.test(password));

}

