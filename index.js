import express from 'express'

import {passwordCheck} from './utilities.js'

const PORT = 5000;


const app = express()
app.use(express.json());


app.get("/password_check", (req, res) => {

  const body = req.body;
  if (!body || !body.password) return res.status(500).send('invalid data')
  return res.status(200).send(passwordCheck(body.password));
});

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})

