# Password Checker (Miniservice)

The 'Dockerized' version of the password-checker tool


# Prepare

 `yarn`


# Run



1. `docker-compose build`

2. `docker-compose up`

This launches the service at `http://localhost:5000/password_check`

To check the password send a GET request with the password in the body, JSON encoded.  The service returns TRUE or FALSE depending on whether the password passes the security criteria.

# Requires

Node v. 15.x, Docker/Docker-Compose
